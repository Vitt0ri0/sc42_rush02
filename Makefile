# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rmanfred <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/02/10 13:59:50 by rmanfred          #+#    #+#              #
#    Updated: 2020/02/17 21:13:23 by emetapod         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = colle-2

NAME2 = librush

NAME3 = libft

SRC_RUSHES = ./librush/

SRS_LIB = ./libft/

SRS_COLLE = ./colle/

FLAG = -Wall -Wextra -Werror

all: $(NAME)

$(NAME):
		gcc -c -Wall -Wextra -Werror ./libft/*.c
		ar rc libft.a *.o
		ranlib libft.a
		#
		gcc -c -Wall -Wextra -Werror -I./libft/ ./librush/rush*.c
		ar rc librush.a *.o
		ranlib librush.a
		#
		
		gcc -I./libft/ -I./librush/ -L. -lrush -L. -lft \
			-o "colle-2" \
			./colle/*.c

		$(MAKE) clean	

runner:
		gcc -I./librush/ -I./libft/ -L. -lft -o rush_runner ./librush/*.c

clean:
	/bin/rm -f *.o

fclean: clean
	/bin/rm -f colle-2
	/bin/rm -f rush_runner
	/bin/rm -f *.a

re: fclean all
