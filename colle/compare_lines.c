#include "header_colle.h"

int		compare_lines(char *line_input, char *line_rush)
{
	int i;

	i = -1;
	while(line_input[++i] && line_rush[i])
		if (line_input[i] != line_rush[i])
			return (0);
	return (1);
}
