/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_colle.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/15 21:02:39 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/17 21:50:01 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header_colle.h"
#include "librush.h"

int RUSHES_AMOUNT = 5;

void	detect_rush_shape(char *line, int *a, int *b);

int		main(void)
{
	char	*input_line;
	char	*rush_line;
	int		i;
	int		len;
	int		wid;
	int		res;
	int 	result[5];

	i = 0;
	len = 0;
	wid = 0;
	input_line = parse_input_line();
	detect_rush_shape(input_line, &len, &wid);
	while (i < RUSHES_AMOUNT)
	{
		rush_line = rush_caller(i, len, wid);
		res = compare_lines(input_line, rush_line);
		result[i] = res;
		i++;
		free(rush_line);
	}
	print_colle_result(result, len, wid);
	return (0);
}

void	detect_rush_shape(char *line, int *a, int *b)
{
	int		i;
	char	c;
	int		len;
	int		wid;

	len = 0;
	wid = 0;
	i = 0;
	while (line[i])
	{
		c = line[i];
		if (c == '\n')
		{
			if (wid == 0)
				wid = i;
			len++;
		}
		i++;
	}
	*a = len;
	*b = wid;
}
