/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_input.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/15 21:22:38 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/16 00:12:02 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header_colle.h"

char	*parse_input_line(void)
{
	char			c;
	char			*line;
	int				pos;
	unsigned int	buf_size;

	buf_size = 2;
	pos = 0;
	line = malloc(sizeof(char) * buf_size);
	while (read(0, &c, 1))
	{
		if (buf_size - pos < 2)
		{
			line = ft_realloc(line, &buf_size);
		}
		line[pos] = c;
		pos++;
	}
	line[pos] = '\0';
	return (line);
}

t_list	*parse_input_list(void)
{
	char	c;
	t_list	*root;

	root = NULL;
	while (read(0, &c, 1) != 0)
	{
		if (!root)
			root = ft_list_create_elem(&c);
		else
			ft_list_push_back(root, &c);
	}
	return (root);
}

char	**parse_input_malloc(void)
{
	int		malloc_len;
	char	c;
	char	**lines;
	int		len;
	int		wid;

	len = 0;
	wid = 0;
	malloc_len = 10;
	lines = (char **)malloc(malloc_len * sizeof(char *));
	while (len < malloc_len)
	{
		lines[len] = (char *)malloc(malloc_len * sizeof(char));
		len++;
	}
	len = 0;
	lines = create_array_empty(malloc_len, malloc_len);
	while (read(0, &c, 1) != 0)
	{
		lines[len][wid] = c;
		if (c == '\n')
		{
			lines[len][wid] = '\0';
			len++;
			wid = 0;
		}
		else
			wid++;
	}
	lines[len][wid] = '\0';
	return (lines);
}
