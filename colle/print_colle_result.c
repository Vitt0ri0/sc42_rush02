/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_colle_result.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/16 01:40:37 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/17 21:23:10 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	print_colle_result(int rushes[], int len, int wid)
{
	int i;
	int length;
	int is_printed;

	is_printed = 0;
	i = 0;
	length = 5;
	while (i < length)
	{
		if (rushes[i] == 1)
		{
			if (!is_printed)
			{
				is_printed = 1;
			} else {
				ft_putstr(" || ");
			}
			if (i < 2)
				ft_putstr("[rush-0");
			else
				ft_putstr("[colle-0");
			ft_putnbr(i);
			ft_putstr("] ");
			ft_putstr("[");
			ft_putnbr(len);
			ft_putstr("] ");
			ft_putstr("[");
			ft_putnbr(wid);
			ft_putstr("]");
		}
		i++;
	}
	if (!is_printed)
		ft_putstr("aucune");
	ft_putstr("\n");
}
