/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colle_print.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfarenga <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/16 18:11:42 by tfarenga          #+#    #+#             */
/*   Updated: 2020/02/16 18:11:56 by tfarenga         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	colle_print_line(char *line)
{
	int		i;

	i = -1;
	while (line[++i])
		ft_putchar(line[i]);
}

void	colle_print_lines(char **lines)
{
	int len;
	int wid;

	len = 0;
	wid = 0;
	while (lines[len][0])
	{
		while (lines[len][wid] != '\0')
		{
			ft_putchar(lines[len][wid]);
			wid++;
		}
		len++;
		wid = 0;
		ft_putchar('\n');
	}
}
