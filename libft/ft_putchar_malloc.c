/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar_malloc.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/16 00:15:43 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/16 00:27:09 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putchar_malloc(char **lines, int *len, int *wid, char c)
{
	int l;
	int w;

	l = *len;
	w = *wid;
	if (c == '\n')
	{
		lines[l][w] = '\0';
		l++;
		w = 0;
	}
	else
	{
		lines[l][w] = c;
		w++;
	}
}
