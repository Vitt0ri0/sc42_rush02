/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_rush.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: emetapod <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/15 17:51:33 by emetapod          #+#    #+#             */
/*   Updated: 2020/02/17 20:45:33 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "librush.h"

int		main(int ac, char **argv)
{
	int prog_name;
	int a;
	int b;

	if (ac == 4)
	{
		prog_name = ft_atoi(argv[1]);
		a = ft_atoi(argv[2]);
		b = ft_atoi(argv[3]);
		if (prog_name == 0)
			rush00(a, b);
		else if (prog_name == 1)
			rush01(a, b);
		else if (prog_name == 2)
			rush02(a, b);
		else if (prog_name == 3)
			rush03(a, b);
		else if (prog_name == 4)
			rush04(a, b);
		return (0);
	}
	if (ac == 5 && argv[4][0] == 'm')
	{
		char *line;

		prog_name = ft_atoi(argv[1]);
		a = ft_atoi(argv[2]);
		b = ft_atoi(argv[3]);

		if (prog_name == 0)
			line = rush00m(a, b);

		colle_print_line(line);
	}

	return (0);
}
