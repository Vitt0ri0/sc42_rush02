/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush00.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alancel <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/02 19:07:57 by alancel           #+#    #+#             */
/*   Updated: 2020/02/02 19:26:27 by alancel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "librush.h"

char	*rush00m(int x, int y)
{
	char *line;
	int p;
	int *pos;
	int i;

	p = 0;
	pos = &p;
	line = ft_malloc_a_by_b(x, y);
	i = 0;
	if (x <= 0 || y <= 0)
	{
		line[*pos] = TERM;
		return line;
	}
	print_line_0(x, 'o', '-', line, pos);
	while (i < y - 2)
	{
		print_line_0(x, '|', ' ', line, pos);
		i++;
	}
	if (y > 1)
	{
		print_line_0(x, 'o', '-', line, pos);
	}
	line[*pos] = TERM;
	return line;
}
