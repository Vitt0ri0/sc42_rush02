/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush01.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfarenga <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/16 19:00:12 by tfarenga          #+#    #+#             */
/*   Updated: 2020/02/16 20:37:34 by tfarenga         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "librush.h"
void	print_char01(int countx, int county, int x, int y)
{
	if (countx == 1 && county == 1)
		ft_putchar('/');
	else if (y != 1 && x != 1 && countx == x && county == y)
		ft_putchar('/');
	else if ((countx == 1 && county == y) || (countx == x && county == 1))
		ft_putchar('\\');
	else if ((countx == 1 || countx == x) && county != 1 && county != y)
		ft_putchar('*');
	else if ((county == 1 || county == y) && countx != 1 && countx != x)
		ft_putchar('*');
	else if (county > 1 && county < y && countx > 1 && countx < x)
		ft_putchar(' ');
}

void	rush01(int x, int y)
{
	int county;
	int countx;

	county = 0;
	while (++county <= y)
	{
		countx = 0;
		while (++countx <= x)
		{
			print_char01(countx, county, x, y);
		}
		ft_putchar('\n');
	}
}
