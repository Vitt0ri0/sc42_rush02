/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush02.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfarenga <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/16 19:00:25 by tfarenga          #+#    #+#             */
/*   Updated: 2020/02/17 20:48:38 by emetapod         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "librush.h"

char	*rush02m(int x, int y)
{
	char *line;
	int p;
	int *pos;
	int i;

	p = 0;
	pos = &p;
	line = ft_malloc_a_by_b(x, y);
	i = 0;
	if (x <= 0 || y <= 0)
	{
		line[*pos] = TERM;
		return line;
	}
	print_line_0(x, 'A', 'B', line, pos);
	while (i < y - 2)
	{
		print_line_0(x, 'B', ' ', line, pos);
		i++;
	}
	if (y > 1)
	{
		print_line_0(x, 'C', 'B', line, pos);
	}
	return (line);
}
