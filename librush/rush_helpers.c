#include "librush.h"

char	*rush_caller(int rush_number, int len, int wid)
{
	char *line;

	line = NULL;
	if (rush_number == 0)
		line = rush00m(len, wid);
	else if (rush_number == 1)
		line = rush01m(len, wid);
	else if (rush_number == 2)
		line = rush02m(len, wid);
	else if (rush_number == 3)
		line = rush03m(len, wid);
	else if (rush_number == 4)
		line = rush04m(len, wid);
	return line;
}

void	ft_putchar_m(char c, char *line, int *pos)
{
	line[*pos] = c;
	++(*pos);
}

void	print_line_0(int length, char a, char b, char *line, int *pos)
{
	int i;

	i = 0;
	ft_putchar_m(a, line, pos);
	while (i < length - 2)
	{
		ft_putchar_m(b, line, pos);
		i++;
	}
	if (length > 1)
	{
		ft_putchar_m(a, line, pos);
	}
	ft_putchar_m('\n', line, pos);
}

void	line0(int length, char a, char b)
{
	int i;

	i = 0;
	ft_putchar(a);
	while (i < length - 2)
	{
		ft_putchar(b);
		i++;
	}
	if (length > 1)
	{
		ft_putchar(a);
	}
	ft_putchar('\n');
}
